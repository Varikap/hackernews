import React, {useEffect, useState, memo} from 'react';

import { getStoryIds } from "../services/hackerNewsAPI";

import { Story } from "../components/Story";

import { GlobalStyle, StoriesContainerWrapper } from "../styles/StoriesContainerStyles";

import { useInfiniteScroll } from "../hooks/useInfiniteScroll";

export const StoriesContainer = () => {
    const { count } = useInfiniteScroll();
    const [storyIds, setStoryIds] = useState([]);

    useEffect(() => {
        getStoryIds().then(data => setStoryIds(data));
    }, [])
  
    return (
        <>
        <GlobalStyle/>
        <StoriesContainerWrapper data-test-id="stories-container">
        <h1>HackerNews Stories</h1>
        {storyIds.slice(0, count).map(storyId => (
          <Story 
            storyId={storyId}
            key={storyId}/>
        ))}
        </StoriesContainerWrapper>
        </>
    );
} 